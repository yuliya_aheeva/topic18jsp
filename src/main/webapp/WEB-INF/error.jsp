<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <h1>Oops!</h1>

    <p>You shouldn't be here.</p>
    <p>Please, agree with terms of service first.</p>
    <p><a href="page1.jsp">Start page</a></p>
</body>
</html>
