<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Page 2</title>
</head>
<body>
    <div align="center">
        <h1 style="margin-top: 250px">Hello <%= session.getAttribute("name") %>!</h1>
            <form  method="POST">
                <p>Make your order:</p>
                    <select name="goods">
                        <option selected value="s1">Book (5.5$)</option>
                        <option value="s2">Pen (0.8$)</option>
                        <option value="s3">Newspaper (1.0$)</option>
                        <option value="s4">Ruler (1.2$)</option>
                    </select><br><br>
                        <input type="submit" name="add" value="Add Item">
                        <input type="submit" name="submit" value="Submit"><br><br>
                You order: <%= session.getAttribute("basket") %>
            </form>
    </div>
</body>
</html>
