package org.sample;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(
    name = "shopServlet",
    urlPatterns = "/shop")
public class ShopServlet extends HttpServlet {
  private HashMap<Item, Integer> basket = new HashMap<>();
  private ArrayList<Item> list = new ArrayList<>();

  public void init() {
    list.add(new Item("Book", 5.5));
    list.add(new Item("Pen", 0.8));
    list.add(new Item("Newspaper", 1.0));
    list.add(new Item("Ruler", 1.2));
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    init();
    HttpSession session = req.getSession();
    Map<String, String[]> params = req.getParameterMap();
    if (params.isEmpty() || !params.containsKey("terms")) {
      RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/error.jsp");
      dispatcher.forward(req, resp);
    } else {
      String name = params.get("name")[0];
      RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/page2.jsp");
      session.setAttribute("name", name);
      dispatcher.forward(req, resp);
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
    String[] price  = req.getParameterValues("goods");
    HttpSession session = req.getSession();

    if (req.getParameter("add") != null) {
      doGet(req, resp);
      for (String s : price) {
        int count = 1;
        if (s.equals("s1")) {
          if (basket.containsKey(list.get(0))) {
            count = basket.get(list.get(0)) + 1;
          }
          basket.put(list.get(0), count) ;
        }
        if (s.equals("s2")) {
          if (basket.containsKey(list.get(1))) {
            count = basket.get(list.get(1)) + 1;
          }
          basket.put(list.get(1), count) ;
        }
        if (s.equals("s3")) {
          if (basket.containsKey(list.get(2))) {
            count = basket.get(list.get(2)) + 1;
          }
          basket.put(list.get(2), count) ;
        }
        if (s.equals("s4")) {
          if (basket.containsKey(list.get(3))) {
            count = basket.get(list.get(3)) + 1;
          }
          basket.put(list.get(3), count) ;
        }
      }
      req.setAttribute("basket", basket);
      session.setAttribute("basket", basket);
    }
    if (req.getParameter("submit") != null) {
      resp.sendRedirect(req.getContextPath() + "/sum");
      session.setAttribute("basket", basket);
      double total = 0;
      for (Map.Entry<Item, Integer> entry : basket.entrySet()) {
        total += entry.getKey().getPrice() * entry.getValue();
      }
      session.setAttribute("total", total);
    }
  }
}
